# Contributor: Francesco Colista <fcolista@alpinelinux.org>
# Maintainer: Francesco Colista <fcolista@alpinelinux.org>
pkgname=bctoolbox
pkgver=5.1.0
pkgrel=0
pkgdesc="Utilities library used by Belledonne Communications softwares like belle-sip, mediastreamer2 and linphone"
url="https://github.com/BelledonneCommunications/bctoolbox"
arch="all"
license="GPL-2.0-or-later"
options="!check" # bcunit not available
makedepends="cmake mbedtls-dev"
subpackages="$pkgname-dev"
source="$pkgname-$pkgver.tar.gz::https://github.com/BelledonneCommunications/bctoolbox/archive/$pkgver.tar.gz"

build() {
	cmake \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_MODULE_PATH=/usr/lib/cmake \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DCMAKE_SKIP_INSTALL_RPATH=ON \
		-DENABLE_MBEDTLS=YES \
		-DENABLE_POLARSSL=NO \
		-DENABLE_STATIC=NO \
		-DENABLE_TESTS_COMPONENT=OFF \
		-DENABLE_SHARED=YES .
	make
}

check() {
	make check
}

package() {
	make DESTDIR="$pkgdir" install
}

dev() {
	default_dev
	mkdir -p "$subpkgdir"/usr/lib/cmake/bctoolbox
	mv "$pkgdir"/usr/share/bctoolbox/cmake/* "$subpkgdir"/usr/lib/cmake/bctoolbox
	# Remove empty dirs
	rmdir "$pkgdir"/usr/share/bctoolbox/cmake
	rmdir "$pkgdir"/usr/share/bctoolbox
	rmdir "$pkgdir"/usr/share
}

sha512sums="
fe158d932eac389519a7446b2daf0451b190da86f3cb89dd79eea83b9ace4acb70627456fb38b07f25db7761d2ef29a9b7cc8005349e940cdc1422520c81c76d  bctoolbox-5.1.0.tar.gz
"
